## Installation of Docker on Centos
*yum install -y yum-utils device-mapper-persistent-data lvm2*	
*yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo*  
*yum install docker-ce*  
*systemctl start docker*	

## Build New Image using docker build command
*docker build -t my-apache2 .*

## List Docker Images
*docker images*

## Run Docker in the deattached mode
*docker run -dit --name webserver -p 80:80 my-apache2*

## Login to Docker Container
*docker exec -it webserver /bin/bash*